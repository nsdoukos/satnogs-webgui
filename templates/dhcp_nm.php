<?php ob_start() ?>
  <input type="submit" class="btn btn-outline btn-primary" value="<?php echo _("Save settings"); ?>" name="savedhcpsettingsnm" />
<?php $buttons = ob_get_clean(); ob_end_clean(); $__template_data["buttons"] = $buttons ?>

<div class="row">
  <div class="col-lg-12">
    <div class="card">

    <div class="card-header">
        <div class="row">
          <div class="col">
            <i class="fas fa-exchange-alt mr-2"></i><?php echo _("DHCP Server"); ?>
          </div>
          <div class="col">
            <button class="btn btn-light btn-icon-split btn-sm service-status float-right">
              <span class="icon text-gray-600"><i class="fas fa-circle service-status-<?php echo $hotspotStatus ?>"></i></span>
              <span class="text service-status">Dnsmasq <?php echo _($hotspotStatus) ?></span>
            </button>
          </div>
        </div><!-- /.row -->
      </div><!-- /.card-header -->

      <div class="card-body">
        <?php $status->showMessages(); ?>
        <form method="POST" action="dhcp_conf_nm" class="js-dhcp-settings-form">
          <?php echo CSRFTokenFieldTag() ?>

          <!-- Nav tabs -->
          <ul class="nav nav-tabs mb-3">
            <li class="nav-item"><a class="nav-link active" href="#server-settings-nm" data-toggle="tab"><?php echo _("Server settings"); ?></a></li>
            <li class="nav-item"><a class="nav-link" href="#logging_nm_dhcp" data-toggle="tab"><?php echo _("Logging"); ?></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <?php echo renderTemplate("dhcp_nm/general_nm", $__template_data) ?>
            <?php echo renderTemplate("dhcp_nm/logging_nm", $__template_data) ?>
          </div><!-- /.tab-content -->

        </form>
      </div><!-- ./ card-body -->


    </div> <!-- .card -->
  </div> <!-- .col-lg-12 -->
</div> <!-- .row -->