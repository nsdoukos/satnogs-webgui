<div class="tab-pane fade" id="security">
  <h4 class="mt-3"><?php echo _("Security settings"); ?></h4>
  <div class="row mb-3">
    <div class="col-md-6">
      <div class="form-group">
        <label for="cbxwpa"><?php echo _("Security type"); ?></label>
        <?php SelectorOptions('wpa', $arrSecurity, $arrConfig['802-11-wireless-security.key-mgmt'], 'cbxwpa', 'toggleWpaFields'); ?>
      </div>
      <fieldset id="wpa-sec">
        <div class="form-group">
          <label for="cbxwpapairwise"><?php echo _("Encryption Type"); ?></label>
          <?php SelectorOptions('wpa_pairwise', $arrEncType, $arrConfig['802-11-wireless-security.pairwise'], 'cbxwpapairwise'); ?>
        </div>
        <label for="txtwpapassphrase"><?php echo _("PSK"); ?></label>
        <div class="input-group">
          <input type="text" class="form-control" id="txtwpapassphrase" name="wpa_passphrase" value="<?php echo htmlspecialchars($arrConfig['802-11-wireless-security.psk'], ENT_QUOTES); ?>" />
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button" id="gen_wpa_passphrase"><i class="fas fa-magic"></i></button>
          </div>
        </div>
      </fieldset>
    </div>
  </div>
  <?php echo $buttons ?>
</div><!-- /.tab-pane | security tab -->