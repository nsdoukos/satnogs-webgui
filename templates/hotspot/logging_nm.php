<div class="tab-pane fade" id="hotspot_logging">
  <!-- Network Manager journalctl table -->
  <h4 class="mt-3"><?php echo _("Journalctl"); ?></h4>
  <div class="card">
    <div class="card-header">
      <?php echo _("Journalctl -r -u NetworkManager.service -n 300 --since \"1 days ago\" | grep $hotspot_ifname") ?>
      <button type="button" class="btn btn-card float-right ml-1 js-hotspot-journal-table-copy" data-toggle="tooltip" data-placement="top" title="Copy"> <i class="fas fa-copy"></i></button>
      <button type="button" class="btn btn-card float-right ml-1 js-hotspot-journal-table-refresh" data-toggle="tooltip" data-placement="top" title="Refresh"> <i class="fas fa-sync-alt"></i></button>
      <button type="button" class="btn btn-card float-right js-journal-nm-table-export" data-toggle="tooltip" data-placement="top" title="Export"> <i class="fas fa-file-export"></i></button>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive" id="hotspot_journalctl_table">
            <table class="table">
              <tbody>
                Journal not generated. Refresh page or table to generate.
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mt-3">
    <div class="col-sm">
      <button type="button" class="btn btn-outline btn-primary js-hotspot-logs-refresh"> <i class="fas fa-sync-alt"></i> <?php echo _("Refresh") ?> </button>
      <button type="button" class="btn btn-outline btn-primary js-hotspot-logs-download"> <i class="fas fa-file-export"></i> <?php echo _("Export") ?> </button>
      <a href="" target="_blank" class="btn btn-outline btn-primary js-hotspot-logs-post"> <i class="fas fa-external-link-alt"></i> <?php echo _("Support") ?></a>
    </div>
  </div>

</div><!-- /.tab-pane logging -->
