<div class="tab-pane active" id="basic">
  <h4 class="mt-3"><?php echo _("Basic settings") ;?></h4>
  <div class="row">
    <div class="form-group col-md-6">
      <label for="cbxinterface"><?php echo _("Interface") ;?></label>
      <?php
        SelectorOptions('interface', $interfaces, $arrConfig['connection.interface-name'], 'cbxinterface');
      ?>
    </div>
  </div>
    <div class="row">
      <div class="form-group col-md-6">
        <label for="txtssid"><?php echo _("SSID"); ?></label>
        <input type="text" id="txtssid" class="form-control" name="ssid" value="<?php echo htmlspecialchars($arrConfig['802-11-wireless.ssid'], ENT_QUOTES); ?>" />
      </div>
  </div>
  <div class="row">
    <div class="form-group col-md-6">
      <label for="cbxhwmode"><?php echo _("Wireless Mode") ;?></label>
      <?php
        $selectedBand = $arrConfig['802-11-wireless.band'];
      SelectorOptions('hw_mode', $arr80211Standard, $selectedBand, 'cbxhwmode', 'loadChannelSelectNM', null); ?>
    </div>
  </div>
  <div class="row">
    <div class="form-group col-md-6">
      <label for="cbxchannel"><?php echo _("Channel"); ?></label>
      <?php
      // This does absolutely nothing aside from creating a class form-control
      // with name and id for js to manipulate
      $selectablechannels = Array();
      SelectorOptions('channel', $selectablechannels, intval($arrConfig['802-11-wireless.channel']), 'cbxchannel'); ?>
    </div>
  </div>
  <?php echo $buttons ?>
</div>
