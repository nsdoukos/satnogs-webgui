<div class="tab-pane fade" id="advanced">
  <h4 class="mt-3"><?php echo _("Advanced settings"); ?></h4>
  <div class="row">
    <div class="col-md-6 mb-2">
      <div class="custom-control custom-switch">
        <?php $checked = $bridgedEnabled == 1 ? 'checked="checked"' : '' ?>
        <input class="custom-control-input" id="chxbridgedenable" name="bridgedEnable" type="checkbox" value="1" <?php echo $checked ?> />
        <label class="custom-control-label" for="chxbridgedenable"><?php echo _("Bridged AP mode"); ?></label>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mb-2">
      <div class="custom-control custom-switch">
        <?php $checked = $arrConfig['802-11-wireless.hidden'] == "yes" ? 'checked="checked"' : '' ?>
        <input class="custom-control-input" id="chxhidden" name="hidden" type="checkbox" value="yes" <?php echo $checked ?> />
        <label class="custom-control-label" for="chxhidden"><?php echo _("Hide SSID in broadcast"); ?></label>
      </div>
    </div>
  </div>
  <?php echo $buttons ?>
</div><!-- /.tab-pane | advanded tab -->
