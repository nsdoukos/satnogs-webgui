<div class="tab-pane fade" id="logging">
  <h4 class="mt-3"><?php echo _("Journalctl"); ?></h4>
  <div class="card h-100 w-100">
    <div class="card-header">
      <?php echo _("Journalctl -u satnogs-client.service -n 300") ?>
      <button class="btn btn-card float-right ml-1 js-journal-table-copy" data-toggle="tooltip" data-placement="top" title="Copy"> <i class="fas fa-copy"></i></button>
      <button class="btn btn-card float-right ml-1 js-journal-table-refresh" data-toggle="tooltip" data-placement="top" title="Refresh"> <i class="fas fa-sync-alt"></i></button>
      <button class="btn btn-card float-right js-journal-table-export" data-toggle="tooltip" data-placement="top" title="Export"> <i class="fas fa-file-export"></i></button>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive" id="journalctl_table">
            <table class="table">
              <tbody>
                Journal not generated. Refresh page or table to generate.
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <h4 class="mt-3"><?php echo _("Support"); ?></h4>
  <div class="card h-100 w-100">
    <div class="card-header">
      <?php echo _("Satnogs client support information") ?>
      <button class="btn btn-card float-right ml-1 js-satnogs-support-table-copy" data-toggle="tooltip" data-placement="top" title="Copy"> <i class="fas fa-copy"></i></button>
      <button class="btn btn-card float-right ml-1 js-satnogs-support-table-refresh" data-toggle="tooltip" data-placement="top" title="Refresh"> <i class="fas fa-sync-alt"></i></button>
      <button class="btn btn-card float-right js-support-table-export" data-toggle="tooltip" data-placement="top" title="Export"> <i class="fas fa-file-export"></i></button>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive" id="satnogs_support_table">
            <table class="table">
              <tbody>
                Satnogs support not generated. Refresh page or table to generate.
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <h4 class="mt-3"><?php echo _("SoapySDR"); ?></h4>
  <div class="card h-100 w-100">
    <div class="card-header">
      <?php echo _("SoapySDRUtil --probe") ?>
      <button class="btn btn-card float-right ml-1 js-soapysdr-table-copy" data-toggle="tooltip" data-placement="top" title="Copy"> <i class="fas fa-copy"></i></button>
      <button class="btn btn-card float-right ml-1 js-soapysdr-table-refresh" data-toggle="tooltip" data-placement="top" title="Refresh"> <i class="fas fa-sync-alt"></i></button>
      <button class="btn btn-card float-right js-soapysdr-table-export" data-toggle="tooltip" data-placement="top" title="Export"> <i class="fas fa-file-export"></i></button>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive" id="soapysdr_table">
            <table class="table">
              <tbody>
                SoapySDR not generated. Refresh page or table to generate.
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row mt-3">
    <div class="col-sm">
      <button class="btn btn-outline btn-primary js-satnogs-logs-refresh"> <i class="fas fa-sync-alt"></i> <?php echo _("Refresh") ?> </button>
      <button class="btn btn-outline btn-primary js-satnogs-logs-download"> <i class="fas fa-file-export"></i> <?php echo _("Export") ?> </button>
      <a href="" target="_blank" class="btn btn-outline btn-primary js-satnogs-logs-post"> <i class="fas fa-external-link-alt"></i> <?php echo _("Support") ?></a>
    </div>
  </div>

</div><!-- /.tab-pane logging -->
