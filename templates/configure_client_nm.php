<div class="row" id="wifiClientContent">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <div class="row align-items-center">
          <div class="col">
            <i class="fas fa-wifi mr-2"></i><?php echo _("WiFi client"); ?>
          </div>
            <div class="col">
              <button class="btn btn-light btn-icon-split btn-sm service-status float-right">
                <span class="icon"><i class="fas fa-circle service-status-<?php echo $ifaceStatus ?>" id="client-status-indicator"></i></span>
                <span class="text service-status" id="client-status-name"><?php echo strtolower($clientInterface) .' '. _($ifaceStatus) ?></span>
              </button>
            </div>
        </div><!-- /.row -->
      </div><!-- /.card-header -->
      <div class="card-body">

        <div id="status_messages_js"></div>
        <div class="row align-items-center">
          <div class="col">
            <h4 class="m-0 text-nowrap"><?php echo _("Client settings"); ?></h4>
          </div>
          <div class="col">
            <button type="button" class="btn btn-info float-right js-reload-wifi-stations-nm"><?php echo _("Rescan"); ?></button>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="js-wifi-stations-nm"></div>
          </div>
        </div>
      </div><!-- ./ card-body -->
    </div><!-- /.card -->
  </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

<!-- Modal -->
<div class="modal fade" id="configureClientModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <div class="modal-title" id="ModalLabel"><i class="fas fa-sync-alt mr-2"></i><?php echo _("Configuring WiFi Client"); ?></div>
      </div>
      <div class="modal-body">
	<div class="col-md-12 mb-3 mt-1"><?php echo _("Configuring Wifi Client Interface..."); ?></div>
        <div class="d-flex justify-content-center">
          <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-outline btn-primary" data-dismiss="modal"><?php echo _("Close"); ?></button>
      </div>
    </div>
  </div>
</div>

