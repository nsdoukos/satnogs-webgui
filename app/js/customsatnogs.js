/**
 * Updates information configuration cards, and alert message on station
 * state (registered | not registered) with ajax request when the Satnogs 
 * Station page is clicked by the user from the navbar. This function does not
 * invoke auto refreshing if the station is not registered.
 */
function setupRegistered() {
    $("#info").addClass('loading-spinner');
    $.ajax({
        url: 'ajax/satnogs/information_station.php',
        type: 'get',
        success: function (response) {
            let alert_msg = get_alert_msg('success');
            $("#satnogs_alert").replaceWith(alert_msg);
            $("#info").html(response);
        },
        error: function (response) {
            let alert_msg = get_alert_msg('error');
            $("#satnogs_alert").replaceWith(alert_msg);
        },
        complete: function () {
            $("#info").removeClass('loading-spinner');
        }
    });

    $("#config").addClass('loading-spinner');
    $.ajax({
        url: 'ajax/satnogs/configuration_station.php',
        type: 'get',
        success: function (response) {
            $("#config").html(response);
        },
        error: function (response) {
            // Alert message is updated from information card, no need to do
            // something here.
        },
        complete: function () {
            $("#config").removeClass('loading-spinner');
        }
    });
    window.sessionStorage.setItem("refresh_timer_started", false);
}

/**
 * Checks for Internet connectivity and connectivity to SatNOGS Network. It 
 * manipulates the connectivity indicator on the Satnogs Station Page.
 */
function getSatnogsNetworkStatus() {
    if (navigator.onLine) {
        $.ajax({
            url: 'ajax/satnogs/satnogs_status_network.php',
            type: 'get',
            success: function (response) {
                if (response.includes('200 OK')) {
                    // Network connectivity and network replied
                    $('#satnogs-network-status-indicator')
                        .removeClass('service-status-down service-status-warning')
                        .addClass('service-status-up')

                    $('#satnogs-network-status-text')
                        .text("SatNOGS Up");
                } else {
                    // Network connectivity and no network reply
                    $('#satnogs-network-status-indicator')
                        .removeClass('service-status-warning service-status-up')
                        .addClass('service-status-down')

                    $('#satnogs-network-status-text')
                        .text("SatNOGS Down");
                }
            }
        });
    } else {
        // No network connectivity
        $('#satnogs-network-status-indicator')
            .removeClass('service-status-up service-status-down')
            .addClass('service-status-warning')

        $('#satnogs-network-status-text')
            .text("No Internet Connectivity");
    }
}

/**
 * Returns html code to update the alert in the satnogs station page.
 * The return value html class can be alert-success if param is success, used
 * when station is registered, alert-warning if param is error, used when station
 * is not registered and alert-warning if param is retry, used when auto refresh
 * of the page is invoked.
 * @param {string} alert - The alert state
 * @returns {string} - The html code to replace the alert.
 */
function get_alert_msg(alert) {
    let msg;
    switch(alert) {
        case 'success':
            msg = `
                <div class="alert alert-success alert-dismissible fade show mt-3" role="alert" id="satnogs_alert">
                Station registered.
                <button type="button" class="close" data-dismiss="alert" arial-label="Close">
                    <span aria-hidden="true">&times;<span>
                </button>
            </div>`;
            break;
        case 'error':
            msg = `
                <div class="alert alert-warning alert-dismissible fade show mt-3" role="alert" id="satnogs_alert">
                Station not registered.
                <button type="button" class="close" data-dismiss="alert" arial-label="Close">
                    <span aria-hidden="true">&times;<span>
                </button>
                </div>`;
            break;
        case 'retry':
            msg = `
                <div class="alert alert-warning alert-dismissible fade show mt-3" role="alert" id="satnogs_alert">
                Station not registered. Retrying<span class="seconds">...</span>
                <button type="button" class="close" data-dismiss="alert" arial-label="Close">
                    <span aria-hidden="true">&times;<span>
                </button>
                </div>`;
            break;
        default:
            alert("Unknown alert message in get_alert_msg! This is meant to be\
                   a check for the developers. If you are not a developer\
                   plz contact the developer team");
    }
    return msg;
}

/**
 * Updates the buttons on information and configuration cards in SatNOGS Station
 * page.
 * @param {string} id - Div id to replace html 
 */
function refresh_info_config_buttons(id) {
    $.ajax({
        url: 'ajax/satnogs/info_config_buttons.php',
        type: 'get',
        success: function(response) {
            $(id).html(response);
        }
    });
}

/**
 * Refresh the information and configuration cards using AJAX requests.
 * If the station is registered cards show the station information and the 
 * "Register" button is replaced with "Edit" and "Refresh" buttons.
 * If the station is not registered the auto refresh timer is started.
 * @see get_alert_msg
 * @see refresh_info_config_buttons
 * @see start_refresh_timer
 */
function refresh_info_config_cards() {
    // Refresh info card
    $.ajax({
        url: 'ajax/satnogs/information_station.php',
        type: 'get',
        success: function(response) {
            let alert_msg = get_alert_msg('success');
            $("#satnogs_alert").replaceWith(alert_msg);
            $("#info").html(response);

            refresh_info_config_buttons("#info_buttons");
        },
        // If not registered alert user and start auto refresh
        error: function(response) {
            let timer_started = window.sessionStorage.getItem("refresh_timer_started");
            if (timer_started === 'false') {
                start_refresh_timer();
            }
        }
    });
    // Refresh config card
    $.ajax({
        url: 'ajax/satnogs/configuration_station.php',
        type: 'get',
        success: function(response) {
            stop_refresh_timer();
            let alert_msg = get_alert_msg('success');
            $("#satnogs_alert").replaceWith(alert_msg);
            $("#config").html(response);

            refresh_info_config_buttons("#config_buttons");
        },
        error: function(response) {
            let timer_started = window.sessionStorage.getItem("refresh_timer_started");
            if (timer_started === 'false') {
                start_refresh_timer();
            }                  
        }
    });
}

/**
 * Start the timer to periodically update the status of the network connectivity
 * @param {int} start_time - Value to countdown in seconds
 * @see getSatnogsNetworkStatus
 */
function start_network_connectivity_timer(start_time) {
    getSatnogsNetworkStatus();
    let network_status_timer = setInterval(function () {
        getSatnogsNetworkStatus();
    }, start_time * 1000);

    window.sessionStorage.setItem("network_status_timer", network_status_timer);
}

/**
 * Stops the timer that periodically updates the status of the network
 * connectivity
 */
function stop_network_connectivity_timer() {
    let network_status_timer = window.sessionStorage.getItem("network_status_timer");
    // Timer might not be started but clearInterval has no problem
    clearInterval(network_status_timer);
}

/**
 * Creates and starts a timer to refresh info and config page every 5s.
 * Information and configuration "Register" button text is updated to indicate
 * that it can be used to stop auto refreshing of the page.
 * @see refresh_info_config_cards
 */
function start_refresh_timer() {
    countdown_alert(5000);
    let refresh_timer = setTimeout(function () {
        refresh_info_config_cards();
        window.sessionStorage.setItem("refresh_timer_started", false);
    }, 5000);
    window.sessionStorage.setItem("refresh_timer", refresh_timer);
    window.sessionStorage.setItem("refresh_timer_started", true);
    $("#info_register_button")
        .html('<i class="fas fa-circle-notch fa-spin"></i> Stop Refreshing');
    $("#config_register_button")
        .html('<i class="fas fa-circle-notch fa-spin"></i> Stop Refreshing');
}

/**
 * Stops and destroys timer refreshing information and configuration page.
 * Information and configuration "Register" button text is updated to register
 * state as the auto refreshing has stopped.
 * @see stop_alert_timer
 */
function stop_refresh_timer() {
    let refresh_timer = window.sessionStorage.getItem("refresh_timer");
    // Timer might not be started but clearInterval has no problem
    clearInterval(refresh_timer);
    window.sessionStorage.setItem("refresh_timer_started", false);
    stop_alert_timer();
    $("#info_register_button")
        .html('<i class="fas fa-external-link-alt"></i> Register');
    $("#config_register_button")
        .html('<i class="fas fa-external-link-alt"></i> Register');
}

/**
 * Creates a timer to update the alert time to refresh page every second.
 * It updates html of the alert message to indicate time remaining till refresh
 * retry.
 * @param {int} start_time - Value to countdown from in ms
 * @see get_alert_msg
 */
function countdown_alert(start_time) {
    var interval_s = start_time / 1000;
    let alert_msg = get_alert_msg('retry');
    $("#satnogs_alert").replaceWith(alert_msg);
    $(".seconds").html(" in " + interval_s-- + "s...");

    var alert_timer = setInterval(function() {
        $(".seconds").html(" in " + interval_s-- + "s...");
        if(interval_s < 0) {
            clearInterval(alert_timer);
            $("#satnogs_alert").replaceWith(alert_msg);

        }
    }, 1000);
    window.sessionStorage.setItem("alert_timer", alert_timer);
}

/**
 * Stops and destroys timer refreshing information and configuration page.
 */
 function stop_alert_timer() {
    let alert_timer = window.sessionStorage.getItem("alert_timer");
    // Timer might not be started but clearInterval has no problem
    clearInterval(alert_timer);
}

/**
 * Gets the text contents of the selector passed and copies it to clipboard
 * using the DEPRECATED function execCommand('copy') by using a temporary
 * element.
 */
function copyText(event) {
	let textarea = document.createElement("textarea");
	textarea.value = $(event.data.id).text();
	document.body.appendChild(textarea);
	textarea.select();
	document.execCommand('copy');
	document.body.removeChild(textarea);
}

/**
 * Updates the journalctl table in SatNOGS Station/Logging tab. The html text is
 * replaced to indicate that it is updated until the ajax request is successful
 */
function getJournalctl() {
    $.ajax({
        url: 'ajax/satnogs/generate_journalctl.php',
        type: 'get',
        beforeSend: function () {
            let msg =
                '<table class="table">\
              <tbody>\
                <p>Generating journal. Please wait...</p>\
              </tbody>\
            </table>'
            $("#journalctl_table").html(msg);
        },
        success: function (response) {
            $("#journalctl_table").html(response);
        }
    });
}

/**
 * Updates the Satnogs Support table in SatNOGS Station/Logging tab. The html
 * text is replaced to indicate that it is updated until the ajax request is
 * successful
 */
function getSatnogsSupport() {
    $.ajax({
        url: 'ajax/satnogs/generate_satnogs_support.php',
        type: 'get',
        beforeSend: function () {
            let msg =
                '<table class="table">\
              <tbody>\
                <p>Generating support report. Please wait...</p>\
              </tbody>\
            </table>'
            $("#satnogs_support_table").html(msg);
        },
        success: function (response) {
            $("#satnogs_support_table").html(response);
        }
    });
}

/**
 * Updates the SoapySDR table in SatNOGS Station/Logging tab. The html text is
 * replaced to indicate that it is updated until the ajax request is successful
 */
function getSoapySdr() {
    $.ajax({
        url: 'ajax/satnogs/generate_soapysdr.php',
        type: 'get',
        beforeSend: function () {
            let msg =
                '<table class="table">\
              <tbody>\
                <p>Generating SoapySDRUtil probe. Please wait...</p>\
              </tbody>\
            </table>'
            $("#soapysdr_table").html(msg);
        },
        success: function (response) {
            $("#soapysdr_table").html(response);
        }
    });
}

/**
 * Generates the list of available channels depending on the band that is being
 * used. The band is read from the HTML Hotspot/Basic page and the channels are
 * added using appropriate selectors. If a selected channel is passed it becomes
 * the selected one, else the first available is displayed.
 * @param {*} selected the channel number to be selected.
 */
function loadChannelSelectNM(selected) {
    // Fetch wireless regulatory data
    $.getJSON("config/wireless.json", function(json) {
        var hw_mode = $('#cbxhwmode').val();
        var channel_select = $('#cbxchannel');
        var data = json["wireless_regdb"];
        var selectablechannels = [...Array(14).keys()].map(i => i + 1);

        // Map selected hw_mode and country to determine channel list
        if (hw_mode === 'a') {
            selectablechannels = data["5Ghz_max48ch"].channels;
        } else {
            selectablechannels = data["2_4GHz_max14ch"].channels;
        }

        // Set channel select with available values
        selected = (typeof selected === 'undefined') ? selectablechannels[0] : selected;
        channel_select.empty();
        $.each(selectablechannels, function(key,value) {
            channel_select.append($("<option></option>").attr("value", value).text(value));
        });

        channel_select.val(selected);
    });
}

/**
 * Gets the operating channel of the hotspot with ajax request and then calls
 * the loadChannelSelectNM function.
 * @see loadChannelSelectNM
 */
function loadChannelNM() {
    $.get('ajax/networking/get_channel_nm.php',function(data){
        jsonData = JSON.parse(data);
        loadChannelSelectNM(jsonData);
    });
}

/**
 * Disable Encryption Type and Password fields in Hotspot/Security page
 * when security type is "Open". Enable otherwise.
 */
function toggleWpaFields()
{
    var wpa = $('#cbxwpa').val();

    // If security type is open disable password and encryption type
    if (wpa === 'open') {
        $('#wpa-sec').prop("disabled", true);
    } else {
        $('#wpa-sec').prop("disabled", false);
    }
}

/**
 * Updates the journalctl table in Hotspot/Logging tab with the output of
 * journalctl for Network Manager unit containing hotspot ifname strings. The
 * html text is replaced to indicate that it is updated until the ajax request
 * is successful
 */
function getHotspotJournalctlNM() {
    $.ajax({
        url: 'ajax/networking/hotspot_generate_nm_journalctl.php',
        type: 'get',
        beforeSend: function () {
            let msg =
                '<table class="table">\
              <tbody>\
                <p>Generating journal. Please wait...</p>\
              </tbody>\
            </table>'
            $("#hotspot_journalctl_table").html(msg);
        },
        success: function (response) {
            $("#hotspot_journalctl_table").html(response);
        }
    });
}

/**
 * Takes the ipv4 method as an argument and manipulates what fields must be
 * visible and what buttons should be checked or disabled.
 * @param {string} method ipv4 method
 */
function changeDHCPServerSettingsFields(method)
{
    // Remove disabled from buttons
    $('#chkdhcp-nm').prop('disabled', false);
    $('#labeldhcp-nm').removeClass('disabled');
    $('#chkstatic-nm').prop('disabled', false);
    $('#labelstatic-nm').removeClass('disabled');
    // Update checked buttons according to ipv4.method
    if (method === 'auto' && !$('#labeldhcp-nm').hasClass('active')) {
        // DHCP button
        $('#labeldhcp-nm').addClass('active');
        $('#chkdhcp-nm').prop('checked', true).trigger('blur');
        // Static button
        $('#labelstatic-nm').removeClass('active');
        $('#chkstatic-nm').prop('checked', false);
        // Shared button
        $('#labelshared-nm').removeClass('active');
        $('#chkshared-nm').prop('checked', false);
        // Static options
        $('#staticoptions-nm').attr('hidden', 'hidden');
    } else if (method === 'manual') {
        // DHCP button
        $('#labeldhcp-nm').removeClass('active');
        $('#chkdhcp-nm').prop('checked', false);
        // Static button
        $('#labelstatic-nm').addClass('active');
        $('#chkstatic-nm').prop('checked', true).trigger('blur');
        // Shared button
        $('#labelshared-nm').removeClass('active');
        $('#chkshared-nm').prop('checked', false);
        // Static options
        $('#staticoptions-nm').removeAttr('hidden');
        $('#gatewayrow-nm').removeAttr('hidden');
    } else if (method === 'shared') {
        // DHCP button
        $('#labeldhcp-nm').removeClass('active');
        $('#chkdhcp-nm').prop('checked', false);
        // Static button
        $('#labelstatic-nm').removeClass('active');
        $('#chkstatic-nm').prop('checked', false);
        // Shared button
        $('#labelshared-nm').addClass('active');
        $('#chkshared-nm').prop('checked', true).trigger('blur');
        // Static options
        $('#staticoptions-nm').removeAttr('hidden');
        $('#gatewayrow-nm').attr('hidden', 'hidden');
        // Hotspot connection
        if ($('#cbxdhcpconnectionnm').val() === "Hotspot") {
            $('#chkdhcp-nm').prop('disabled', true);
            $('#labeldhcp-nm').addClass('disabled');
            $('#chkstatic-nm').prop('disabled', true);
            $('#labelstatic-nm').addClass('disabled');
        }
    }
}

/**
 * Updates the values of Static IP options input fields according to the options
 * passed from an array.
 * 
 * @param {Array} options Array with IP options
 */
function updateDHCPServerSettingsStaticOptions(options)
{
    $('#txtipaddress-nm').val(options['ipv4.addresses']);
    $('#txtgateway-nm').val(options['ipv4.gateway']);
}

/**
 * Reads NM connection ipv4 settings and updates DHCP Server/Server Settings
 * fields 
 * @see changeDHCPServerSettingsFields
 * @see updateDHCPServerSettingsStaticOptions
 */
function loadConnectionDHCPSelect()
{
    conn_name = $('#cbxdhcpconnectionnm').val();

    $.ajax({
        type: 'POST',
        url: 'ajax/networking/get_connection_ipv4_settings.php',
        data: JSON.stringify({conn_name: conn_name}),
        contentType: 'application/json; charset=utf-8',
        success: function(json) {
            json = JSON.parse(json);
            changeDHCPServerSettingsFields(json['ipv4.method']);
            updateDHCPServerSettingsStaticOptions(json);
        }
    });
}

/**
 * Updates the logging table in DHCP Server/Logging tab with the output of
 * journalctl for Network Manager unit containing dhcp or dns strings.
 */
function getDnsDhcpJournalctl() {
    $.ajax({
        url: 'ajax/networking/generate_dns_dhcp_journalctl.php',
        type: 'get',
        beforeSend: function () {
            let msg =
                '<table class="table">\
              <tbody>\
                <p>Generating journal. Please wait...</p>\
              </tbody>\
            </table>'
            $("#nm_dhcp_journalctl_table").html(msg);
        },
        success: function (response) {
            $("#nm_dhcp_journalctl_table").html(response);
        }
    });
}

/**
 * Reloads available wifi stations in Wifi Client page. While the stations are
 * loading a loading spinner is added
 */
function loadWifiStationsNM() {
    $('.js-wifi-stations-nm')
        .empty()
        .addClass('d-flex justify-content-center')
	.html('<div class="spinner-border" role="status"> \
  <span class="sr-only">Loading...</span> \
</div>');


    $.ajax({
        url: 'ajax/networking/wifi_stations_nm.php',
        type: 'get',
        success: function(response) {
            $('.js-wifi-stations-nm').removeClass('d-flex justify-content-center');
            $('.js-wifi-stations-nm').html(response);
        }
    });
}

/**
 * Updates all tables in SatNOGS Station/Logging by calling the functions that
 * updates each table
 * @see getJournalctl
 * @see getSatnogsSupport
 * @see getSoapySdr
 */
function setupSupportInformation() {
    getJournalctl();
    getSatnogsSupport();
    getSoapySdr();
}

/**
 * Updates all tables in Hotspot/Logging by calling the functions that updates
 * each table
 * @see getHotspotJournalctlNM
 */
function setupHotspotSupportInformation() {
    getHotspotJournalctlNM();
}

/**
 * Updates all tables in DHCP Server/Logging by calling the functions that
 * updates each table.
 * @see getDnsDhcpJournalctl
 */
function setupDHCPSupportInformation() {
    getDnsDhcpJournalctl();
}

/**
 * Calls necessary functions when page a specific page is loaded
 * @see setupRegistered
 * @see start_network_connectivity_timer
 * @see loadWifiStationsNM
 * @see loadChannelNM
 * @see toggleWpaFields
 * @see loadConnectionDHCPSelect
 */
function contentLoaded() {
    pageCurrent = window.location.href.split("/").pop();

    switch(pageCurrent) {
        case "satnogs_station":
            setupRegistered();
            start_network_connectivity_timer(20);
            break;
        case "wpa_conf_nm":
            loadWifiStationsNM();
            break;
        case "hotspot_conf_nm":
            loadChannelNM();
            toggleWpaFields();
            break;
        case "dhcp_conf_nm":
            loadConnectionDHCPSelect();
    }
}

/**
 * On "Register" button click, from information or configuration card, opens a
 * new tab with the link to register the station. The auto refresh timer is also
 * started and the button is updated to indicate that the page is refreshing.
 * If the auto refresh timer is already started on "Stop Refreshing" button 
 * click the timer is stopped and the button is updated to indicate that the 
 *  user can use it to register the station.
 */
$(document).on("click", ".js-satnogs-register", function() {
    if ($("#info_register_button").text().includes("Register")||
        $("#config_register_button").text().includes("Register")) {
        $.ajax({
            async: false,
            url: 'ajax/satnogs/register_station.php',
            type: 'get',
            dataType: 'json',
            success: function(response) {
                window.open(response["url"], "_blank");
                refresh_info_config_cards();
            }
        });
    }
    else if ($("#info_register_button").text().includes("Stop Refreshing")||
             $("#config_register_button").text().includes("Stop Refreshing")){
        let alert_msg = get_alert_msg('error');
        $("#satnogs_alert").replaceWith(alert_msg);
        stop_refresh_timer();
    }
});

/**
 * Stops the auto-refresh timer and deletes the alert message div in "Satnogs 
 * Station" page.
 */
$(document).on("click", ".close", function() {
    let timer_started = window.sessionStorage.getItem("refresh_timer_started");
    if (timer_started === 'true') {
        stop_refresh_timer();
    }
});

/**
 * Information and configuration cards are emptied and buttons are refresh 
 * before the are refreshed with the refresh_info_config_cards function. 
 */
$(document).on("click", ".js-satnogs-info-refresh", function () {
    $("#info").html(' <hr> ');
    refresh_info_config_buttons("#info_buttons");
    $("#config").html(' <hr> ');
    refresh_info_config_buttons("#config_buttons");
    refresh_info_config_cards();
});

/**
 * On click event handler that refreshes all tables in SatNOGS Station/Logs
 * @see setupSupportInformation
 */
$(document).on("click", ".js-satnogs-logs-refresh", setupSupportInformation);

/**
 * On click event handler that copies the journalctl table is SatNOGS Station/
 * Logs
 * @see copyText
 */
$(document).on("click", ".js-journal-table-copy", {id: "#journalctl_table"}, copyText);

/**
 * On click event handler that copies the support table is SatNOGS Station/
 * Logs
 * @see copyText
 */
$(document).on("click", ".js-satnogs-support-table-copy", {id: "#satnogs_support_table"}, copyText);

/**
 * On click event handler that copies the SoapySDR table is SatNOGS Station/
 * Logs
 * @see copyText
 */
$(document).on("click", ".js-soapysdr-table-copy", {id: "#soapysdr_table"}, copyText);

/**
 * On click event handler that refreshes the journactl table in SatNOGS Station/
 * Logs
 * @see getJournalctl
 */
$(document).on("click", ".js-journal-table-refresh", getJournalctl);

/**
 * On click event handler that refreshes satnogs support table in
 * SatNOGS Station/Logs
 * @see getSatnogsSupport
 */
$(document).on("click", ".js-satnogs-support-table-refresh", getSatnogsSupport);

/**
 * On click event handler that refreshes soapysdr table in SatNOGS Station/Logs
 * @see getSoapySdr
 */
$(document).on("click", ".js-soapysdr-table-refresh", getSoapySdr);

/**
 * On click event handler that copies the Journalctl table is Hotspot/Logging
 * @see copyText
 */
$(document).on("click", ".js-hotspot-journal-table-copy", {id: "#hotspot_journalctl_table"}, copyText);

/**
 * On click event handler that refreshes journalctl table in Hotspot/Logging
 * @see getHotspotJournalctlNM
 */
$(document).on("click", ".js-hotspot-journal-table-refresh",
    getHotspotJournalctlNM);

/**
 * On click event handler that refreshes all tables in Hotspot/Logging
 * @see setupHotspotSupportInformation
 */
$(document).on("click", ".js-hotspot-logs-refresh",
    setupHotspotSupportInformation);

/**
 * On click event handler that copies the Journalctl table is DHCP Server/Logging
 * @see copyText
 */
$(document).on("click", ".js-nm-dhcp-journal-table-copy", {id: "#nm_dhcp_journalctl_table"}, copyText);

/**
 * On click event handler that refreshes the journactl table in
 * DHCP Server/Logging
 * @see getDnsDhcpJournalctl
 */
$(document).on("click", ".js-nm-dhcp-journal-table-refresh",
    getDnsDhcpJournalctl);

$(document).on("click", ".js-dhcp-logs-refresh",
    setupDHCPSupportInformation);

/**
 * On click event handler that reloads the available wifi stations in Wifi
 * client page
 * @see loadWifiStationsNM
 */
$(".js-reload-wifi-stations-nm").on("click", loadWifiStationsNM);


/**
 * Download the data provided in a file with filename the 
 * [current date-file_suffix]. A new anchor element is created and automatically
 * clicked for the download to start.
 * @param {*} data - the data to be included in the data
 * @param {string} file_suffix - file suffix added to downloaded file
 */
function hiddenDownload (data, file_suffix)
{
    let log = new Blob([data], {type: 'text/plain'});

    let date = new Date().toJSON();

    let hiddenElement = document.createElement('a');
    hiddenElement.href = window.URL.createObjectURL(log);
    hiddenElement.download = date + '_' + file_suffix;
    hiddenElement.click();
    hiddenElement.remove();
}

/**
 * On click event handler for export journalctl button in SatNOGS Server/Logs.
 * Downloads the table as a txt file. If the table is not already generated it
 * is by calling getJournalctl.
 * @see hiddenDownload
 * @see getJournalctl
 */
$(".js-journal-table-export").on("click", function () {
    let title = "Journalctl\n----------\n";
    let log = $("#journalctl_table .unstyled").text();
    let file_suffix = 'journalctl.txt'

    if(!log) {
        getJournalctl();
        $(document).one("ajaxSuccess", function (event, xhr, settings) {
            if (settings.url === 'ajax/satnogs/generate_journalctl.php') {
                log = $("#journalctl_table .unstyled").text();
                let data = title + log;
                hiddenDownload(data, file_suffix);
            }
        });
    }
    else {
        let data = title + log;
        hiddenDownload(data, file_suffix);
    }
});

/**
 * On click event handler for export satnogs support button in 
 * SatNOGS Server/Logs.
 * Downloads the table as a txt file. If the table is not already generated it
 * is by calling getSatnogsSupport.
 * @see hiddenDownload
 * @see getSatnogsSupport
 */
$(".js-support-table-export").on("click", function () {
    let title = "Support\n----------\n";
    let log = $("#satnogs_support_table .unstyled").text();
    let file_suffix = 'support.txt'

    if(!log) {
        getSatnogsSupport();
        $(document).one("ajaxSuccess", function (event, xhr, settings) {
            if (settings.url === 'ajax/satnogs/generate_satnogs_support.php') {
                log = $("#satnogs_support_table .unstyled").text();
                let data = title + log;
                hiddenDownload(data, file_suffix);
            }
        });
    }
    else {
        let data = title + log;
        hiddenDownload(data, file_suffix);
    }
});

/**
 * On click event handler for export soapysdr button in SatNOGS Server/Logs.
 * Downloads the table as a txt file. If the table is not already generated it
 * is by calling getSatnogsSupport.
 * @see hiddenDownload
 * @see getSoapySdr
 */
$(".js-soapysdr-table-export").on("click", function () {
    let title = "SoapySDR\n----------\n";
    let log = $("#soapysdr_table .unstyled").text();
    let file_suffix = 'soapysdr.txt'

    if(!log) {
        getSoapySdr();
        $(document).one("ajaxSuccess", function (event, xhr, settings) {
            if (settings.url === 'ajax/satnogs/generate_soapysdr.php') {
                log = $("#soapysdr_table .unstyled").text();
                let data = title + log;
                hiddenDownload(data, file_suffix);
            }
        });
    }
    else {
        let data = title + log;
        hiddenDownload(data, file_suffix);
    }
});

/**
 * On click event handler for export journalctl button in Hotspot/Logging.
 * Downloads the table as a txt file. If the table is not already generated it
 * is by calling getSatnogsSupport.
 * @see hiddenDownload
 * @see getHotspotJournalctlNM
 */
$(".js-journal-nm-table-export").on("click", function () {
    let title = "Journalctl\n----------\n";
    let log = $("#hotspot_journalctl_table .unstyled").text();
    let file_suffix = 'journalctlNM.txt'

    if(!log) {
        getHotspotJournalctlNM();
        // Create event listener to wait for table generation
        $(document).one("ajaxSuccess", function (event, xhr, settings) {
            if (settings.url == 'ajax/networking/hotspot_generate_nm_journalctl.php') {
                log = $("#hotspot_journalctl_table .unstyled").text();
                let data = title + log;
                hiddenDownload(data, file_suffix);
            }
        });
    }
    else {
        let data = title + log;
        hiddenDownload(data, file_suffix);
    }
});

/**
 * On click event handler for export journalctl button in DHCP Server/Logging.
 * Downloads the table as a txt file. If the table is not already generated it
 * is by calling getDnsDhcpJournalctl.
 * @see getDnsDhcpJournalctl
 * @see hiddenDownload
 */
$(".js-nm-dhcp-journal-table-export").on("click", function (e) {
    e.preventDefault();

    let title = "Journalctl\n----------\n";
    let log = $("#nm_dhcp_journalctl_table .unstyled").text();
    let file_suffix = 'journalctlDnsDhcp.txt'

    if (!log) {
        getDnsDhcpJournalctl(e);
        // Create event listener to wait for table generation
        $(document).one("ajaxSuccess", function (event, xhr, settings) {
            if (settings.url == 'ajax/networking/generate_dns_dhcp_journalctl.php') {
                log = $("#nm_dhcp_journalctl_table .unstyled").text();
                let data = title + log;
                hiddenDownload(data, file_suffix);
            }
        });
    } else {
        let data = title + log;
        hiddenDownload(data, file_suffix);
    }
});

/**
 * Reads the contents of the log tables in SatNOGS Server/Logs card and adds
 * them to a string to be returned. If no table is generated an empty string
 * is returned.
 * @returns {string} log - contents of log tables as string
 */
function createSatnogsLogs() {
    let log="";

    // Journalctl Section
    let title = "Journalctl\n----------\n";    
    let journalctl = "";
    journalctl = $("#journalctl_table .unstyled").text();

    if (journalctl) {
        log = title + journalctl + "\n";
    }
    
    // Support Section
    title = "\n\nSupport\n--------\n"
    let support = "";
    support = $("#satnogs_support_table .unstyled").text();

    if (support) {
        log += title + support + "\n";
    }

    // SoapySdr Section
    title = "\n\nSoapySDR\n--------\n"
    let soapysdr = "";
    soapysdr = $("#soapysdr_table .unstyled").text();

    if (soapysdr) {
        log += title + soapysdr + "\n";
    }

    return log;
}

/**
 * On click event handler for export button in SatNOGS Server/Logs. If no table
 * is generated an alert pop up informs the user and no file is downloaded.
 * @see createSatnogsLogs
 */
$(document).on("click", ".js-satnogs-logs-download", function () {
    let log = createSatnogsLogs();
    // If logs are empty alert user and return
    if (!log) {
        let alert_msg = "All logs are empty!\n\
No file is downloaded. Generate a log and try again.";
        alert (alert_msg);
        return;
    }
    let data = new Blob([log], {type: 'text/plain'});

    let date = new Date().toJSON();
    let hiddenElement = document.createElement('a');

    hiddenElement.href = window.URL.createObjectURL(data);
    hiddenElement.download = date + '-log.txt';
    hiddenElement.click();
});

/**
 * On click event handler for support button in SatNOGS Server/Logs. Opens a new
 * tab with a new post in the community forum. Log fields must be filled by
 * user.
 */
$(".js-satnogs-logs-post").on("click", function (e) {
    e.preventDefault();

    let station_name = document.getElementById("info").childNodes[3].innerText;
    let title = '[Client Support]: ' + station_name;

    log = '**Journalctl**\n```\n\
Paste your Journalctl output here\n```\n\n\
**Support Log**\n```\n\
Paste your Support Log output here\n```\n\n\
**SoapySDR**\n```\n\
Paste your SoapySDR output here\n```';

    community_url = 'https://community.libre.space/new-topic?';
    title = 'title=' + encodeURIComponent(title);
    body = '&body=' + encodeURIComponent(log);
    category = '&category=satnogs';

    url = community_url + title + body + category;
    window.open(url, "_blank");
});

/**
 * Reads the contents of the log tables in Hotspot/Logging card and adds them to
 * a string to be returned. If no table is generated an empty string is
 * returned.
 * @returns {string} log - contents of log tables as string
 */
function createHotspotLogs() {
    let log="";

    // Journalctl Section
    let title = "Journalctl\n----------\n";
    let journalctl = "";
    journalctl = $("#hotspot_journalctl_table .unstyled").text();

    if (journalctl) {
        log = title + journalctl + "\n";
    }

    return log;
}

/**
 * On click event handler for export button in Hotspot/Logging. If no table
 * is generated an alert pop up informs the user and no file is downloaded.
 * @see createHotspotLogs
 */
$(document).on("click", ".js-hotspot-logs-download", function () {
    let log = createHotspotLogs();
    // If logs are empty alert user and return
    if (!log) {
        let alert_msg = "All logs are empty!\n\
No file is downloaded. Generate a log and try again.";
        alert (alert_msg);
        return;
    }
    let data = new Blob([log], {type: 'text/plain'});

    let date = new Date().toJSON();
    let hiddenElement = document.createElement('a');

    hiddenElement.href = window.URL.createObjectURL(data);
    hiddenElement.download = date + '-log.txt';
    hiddenElement.click();
});

/**
 * On click event handler for support button in Hotspot/Logging. Opens a new
 * tab with a new post in the community forum. Log fields must be filled by
 * user.
 */
$(".js-hotspot-logs-post").on("click", function (e) {
    e.preventDefault();

    let title = '[Hotspot Support]: ENTER STATION NAME HERE';

    log = '**Journalctl**\n```\n\
Paste your Journalctl output here\n```\n\n';

    community_url = 'https://community.libre.space/new-topic?';
    title = 'title=' + encodeURIComponent(title);
    body = '&body=' + encodeURIComponent(log);
    category = '&category=satnogs';

    url = community_url + title + body + category;
    window.open(url, "_blank");
});

/**
 * Reads the contents of the log tables in DHCP Server/Logging card and adds
 * them to a string to be returned. If no table is generated an empty string is
 * returned.
 * @returns {string} log - contents of log tables as string
 */
function createDnsDhcpLogs (){
    let log="";

    // Journalctl Section
    let title = "Journalctl\n----------\n";
    let journalctl = "";
    journalctl = $("#nm_dhcp_journalctl_table .unstyled").text();

    if (journalctl) {
        log = title + journalctl + "\n";
    }

    return log;
}

/**
 * On click event handler for export button in Hotspot/Logging. If no table
 * is generated an alert pop up informs the user and no file is downloaded.
 * @see createHotspotLogs
 */
$(document).on("click", ".js-dhcp-logs-download", function () {
    let log = createDnsDhcpLogs();
    // If logs are empty alert user and return
    if (!log) {
        let alert_msg = "All logs are empty!\n\
No file is downloaded. Generate a log and try again.";
        alert (alert_msg);
        return;
    }
    let data = new Blob([log], {type: 'text/plain'});

    let date = new Date().toJSON();
    let hiddenElement = document.createElement('a');

    hiddenElement.href = window.URL.createObjectURL(data);
    hiddenElement.download = date + '-log.txt';
    hiddenElement.click();
});

/**
 * Updates the client service status icon and text in Wifi client card header
 */
function update_client_service_status()
{
    let client_ifname = $('#client-status-name').text().split(' ')[0];
   
    $.ajax({
        type: 'POST',
        url: 'ajax/networking/get_client_status.php',
        data: JSON.stringify({ ifname: client_ifname }),
        contentType: 'application/json; charset=utf-8',
        complete: function(response) {
            $('#client-status-indicator')
	        .removeClass("service-status-up service-status-down")
	        .addClass("service-status-" + response.responseText);
	    $('#client-status-name')
	        .text(client_ifname + ' ' + response.responseText);
	}
    })
}

/**
 * On click event handler for update button in WiFi client. Updates the
 * connection settings.
 */
$(document).on("click", ".js-update-connection-nm", function(e) {
    let button = $(e.target);

    let index = button[0].name.split("_")[1];
    let password = $(`[name=passphrase_${index}]`)[0].value;
    let ssid = this.value;

    $.ajax({
        type: 'POST',
        url: 'ajax/networking/update-nm-connection.php',
        data: JSON.stringify({ ssid: ssid, password: password}),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
            status_msg = '<div class="alert alert-';
            $('#configureClientModal').modal('show');
        },
        success: function() {
            status_msg += 'success">Wifi settings updated successfully';
        },
        error: function() {
            status_msg += 'danger">Wifi settings failed to be updated';
        },
        complete: function() {
            $('#configureClientModal').modal('hide');
            status_msg += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button></div>';
            $('#status_messages_js').html(status_msg);
        }
    })
});

/**
 * On click event handler for disconnect button in WiFi client. Tries to
 * disconnectn and creates an appropriate modal message. The available
 * connections are updated with loadWifiStationsNM
 * @see loadWifiStationsNM
 */
$(document).on("click", ".js-disconnect-nm", function(e) {
    let button = $(e.target);

    let index = button[0].name.split("_")[1];
    ssid = this.value;

    $.ajax({
        url: 'ajax/networking/disconnect-nm.php',
        beforeSend: function () {
            status_msg = '<div class="alert alert-';
            $('#configureClientModal').modal('show');
        },
        success: function() {
            status_msg += 'success">Disconnected from network ssid';
        },
        error: function() {
            status_msg += 'danger">Failed to disconnect from network';
        },
        complete: function() {
            $('#configureClientModal').modal('hide');
            status_msg += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button></div>';
            $('#status_messages_js').html(status_msg);
            loadWifiStationsNM();
            update_client_service_status();
        }
    })
});

/**
 * On click event handler for connect button in WiFi client. Tries to connect
 * to the connection and creates an appropriate modal message. The available
 * connections are updated with loadWifiStationsNM
 * @see loadWifiStationsNM
 */
$(document).on("click", ".js-connect-nm", function(e) {
    let button = $(e.target);

    let index = button[0].name.split("_")[1];
    let password = $(`[name=passphrase_${index}]`)[0].value;
    let ssid = this.value;

    $.ajax({
        type: 'POST',
        url: 'ajax/networking/connect-nm.php',
        data: JSON.stringify({ ssid: ssid, password: password}),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
            status_msg = '<div class="alert alert-';
            $('#configureClientModal').modal('show');
        },
        success: function() {
            status_msg += 'success">New network selected';
        },
        error: function() {
            status_msg += 'danger">Failed to connect to new network';
        },
        complete: function() {
            $('#configureClientModal').modal('hide');
            status_msg += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button></div>';
            $('#status_messages_js').html(status_msg);
            loadWifiStationsNM();
            update_client_service_status();
        }
    })
});

$(document).on("click", ".js-delete-connection-nm", function(e) {
    let button = $(e.target);

    let index = button[0].name.split("_")[1];
    let password = $(`[name=passphrase_${index}]`)[0].value;
    let ssid = this.value;

    $.ajax({
        type: 'POST',
        url: 'ajax/networking/delete-connection-nm.php',
        data: JSON.stringify({ ssid: ssid}),
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
            status_msg = '<div class="alert alert-';
            $('#configureClientModal').modal('show');
        },
        success: function() {
            status_msg += 'success">Connection deleted';
        },
        error: function() {
            status_msg += 'danger">Failed to delete connection';
        },
        complete: function() {
            $('#configureClientModal').modal('hide');
            status_msg += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button></div>';
            $('#status_messages_js').html(status_msg);
            loadWifiStationsNM();
            update_client_service_status();
        }
    })
});

/**
 * When radio button is pressed it behaves differently than when label is
 * pressed so we have to handle it. Use the same event for radio button and
 * label. Also stop clicking the label when button is disabled
 */
$('#chkdhcp-nm, #labeldhcp-nm').on('click', function(e) {
    e.stopPropagation();
    // Check if disabled
    if (!$('#chkdhcp-nm').prop('disabled')) {
        changeDHCPServerSettingsFields('auto');
    } 
});

/**
 * When radio button is pressed it behaves differently than when label is
 * pressed so we have to handle it. Use the same event for radio button and
 * label. Also stop clicking the label when button is disabled
 */
$('#chkstatic-nm, #labelstatic-nm').on('click', function(e) {
    e.stopPropagation();
    // Check if disabled
    if (!$('#chkstatic-nm').prop('disabled')) {
        changeDHCPServerSettingsFields('manual');
    }
});

/**
 * When radio button is pressed it behaves differently than when label is
 * pressed so we have to handle it. Use the same event for radio button and
 * label.
 */
$('#chkshared-nm, #labelshared-nm').on('click', function(e) {
    changeDHCPServerSettingsFields('shared');
});

/**
 * Generates a random password with alphanumeric characters of passed length.
 * @param {Number} pwdLen number of characters for password
 * @returns {String} the generated password
 */
function genPassword(pwdLen) {
    var pwdChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var rndPass = Array(pwdLen).fill(pwdChars).map(function(x) { return x[Math.floor(Math.random() * x.length)] }).join('');
    return rndPass;
}

/**
 * On click event listener for generate random password button
 * @see genPassword
 */
$(document).on("click", "#gen_wpa_passphrase", function(e) {
    $('#txtwpapassphrase').val(genPassword(63));
});

// Define themes
var themes = {
    "default": "custom.php",
    "hackernews" : "hackernews.css",
    "lightsout" : "lightsout.php",
    "material-light" : "material-light.php",
    "material-dark" : "material-dark.php",
}

/**
 * Theme select event handler. Changes the theme cookie according to
 * #theme-select dropdown menu.
 */
$(function() {
    $('#theme-select').change(function() {
        var theme = themes[$( "#theme-select" ).val() ];

        var hasDarkTheme = theme === 'custom.php' ||
            theme === 'material-light.php';
        var nightModeChecked = $("#night-mode").prop("checked");

        if (nightModeChecked && hasDarkTheme) {
            if (theme === "custom.php") {
                set_theme("lightsout.php");
            } else if (theme === "material-light.php") {
                set_theme("material-dark.php");
            }
        } else {
            set_theme(theme);
        }
   });
});

/**
 * Sets cookie name for theme
 * @param {String} theme theme name
 */
function set_theme(theme) {
    $('link[title="main"]').attr('href', 'app/css/' + theme);
    // persist selected theme in cookie 
    setCookie('theme',theme,90);
}

/**
 * Night mode change event handler. Changes the theme cookie according to
 * #night-mode is checked or not.
 */
$(function() {
    var currentTheme = getCookie('theme');
    // Check if the current theme is a dark theme
    var isDarkTheme = currentTheme === 'lightsout.php' || currentTheme === 'material-dark.php';

    $('#night-mode').prop('checked', isDarkTheme);
    $('#night-mode').change(function() {
        var state = $(this).is(':checked');
        var currentTheme = getCookie('theme');

        if (state == true) {
            if (currentTheme == 'custom.php') {
                set_theme('lightsout.php');
            } else if (currentTheme == 'material-light.php') {
                set_theme('material-dark.php');
            }
        } else {
            if (currentTheme == 'lightsout.php') {
                set_theme('custom.php');
            } else if (currentTheme == 'material-dark.php') {
                set_theme('material-light.php');
            }
        }
   });
});

/**
 * Creates a new cookie.
 * @param {String} cname cookie name
 * @param {*} cvalue cookie value
 * @param {Integer} exdays days untill expiration
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Returns the cookie value for the cookie name specified by the caller or null.
 * @param {String} cname 
 * @returns {String} the cookie value
 */
function getCookie(cname) {
    var regx = new RegExp(cname + "=([^;]+)");
    var value = regx.exec(document.cookie);
    return (value != null) ? decodeURI(value[1]) : null;
}

/**
 * Enable Bootstrap tooltips
 */
$(function () {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: 'hover'
    })
})

// Adds active class to current nav-item
$(window).bind("load", function() {
    var url = window.location;
    $('ul.navbar-nav a').filter(function() {
      return this.href == url;
    }).parent().addClass('active');
});

$(document).on("click", ".js-toggle-password", function(e) {
    var button = $(e.target)
    var field  = $(button.data("target"));

    if (field.is(":input")) {
        e.preventDefault();

        if (!button.data("__toggle-with-initial")) {
            $("i", this).removeClass("fas fa-eye").addClass(button.attr("data-toggle-with"));
        }

        if (field.attr("type") === "password") {
            field.attr("type", "text");
        } else {
            $("i", this).removeClass("fas fa-eye-slash").addClass("fas fa-eye");
            field.attr("type", "password");
        }
    }
});

$(document).on("click", ".js-toggle-password-multiple", function(e) {
    var button = $(e.target)
    var field  = $(button.data("target"));
    var scope = "button[id='"+button.attr("id")+"']";

    if (field.is(":input")) {
        e.preventDefault();

        if (!button.data("__toggle-with-initial")) {
            $("i", scope).removeClass("fas fa-eye").addClass(button.attr("data-toggle-with"));
        }

        if (field.attr("type") === "password") {
            field.attr("type", "text");
        } else {
            $("i", scope).removeClass("fas fa-eye-slash").addClass("fas fa-eye");
            field.attr("type", "password");
        }
    }
});

$(document).ready(contentLoaded)

function setCSRFTokenHeader(event, xhr, settings) {
    var csrfToken = $('meta[name=csrf_token]').attr('content');
    if (/^(POST|PATCH|PUT|DELETE)$/i.test(settings.type)) {
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
    }
}

$(document).on("ajaxSend", setCSRFTokenHeader);
