<?php

require_once 'config.php';
require_once 'nm_functions.php';

/**
 * Display hotspot page with nm
 */
function DisplayHotspotConfigNM()
{

    if (isset($_SESSION['status_messages'])) {
        $status = unserialize($_SESSION['status_messages']);
        unset($_SESSION['status_messages']);
    } else {
        $status = new \RaspAP\Messages\StatusMessage;
    }

    $arrConfig = getConnectionSettings(RASPI_NM_HOTSPOT_CONNECTION_NAME);

    $interfaces = getWirelessInterfaces();
    $arr80211Standard = [
        'a' => '802.11a/h/j/n/ac/ax - 5 GHz',
        'bg' => '802.11b/g/n/ax - 2.4 GHz'
    ];

    $arrSecurity = [
        'open' => 'Open',
        'wpa-psk' => 'WPA/WPA2 Personal',
        'sae' => 'WPA3 Personal'];

    $arrEncType = [
        'tkip' => 'TKIP',
        'ccmp' => 'CCMP',
        '' => 'TKIP+CCMP'];

    $bridgedEnabled = checkConnectionIsActive(RASPI_NM_BRIDGE_MASTER_CONNECTION_NAME);
    $bridgedEnabled = strcmp($bridgedEnabled, "down") ? 1 : 0;

    $hotspot_ifname = $arrConfig["connection.interface-name"];

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        // POST StartHotspot or RestartHotspot
        if (isset($_POST['StartHotspot']) || isset($_POST['RestartHotspot'])) {
            // Update hotspot connection settings and create alert message
            $arrConfig = updateConnectionSettings($arrConfig, $status);

            // Disconnect interface and prevent autoconnect
            disconnectInterface($arrConfig["connection.interface-name"]);

            // Activate Hotspot connection and create message
            $result = activateConnection(RASPI_NM_HOTSPOT_CONNECTION_NAME);
            if (strpos($result, 'successfully') === false) {
                $status->addMessage("Error activating Hotspot: " . $result,
                                    'danger');
            }
            else {
                $status->addMessage("Hotspot Activated", 'info');
            }
        }
        // POST StopHotspot
        if (isset($_POST['StopHotspot'])) {
            // Disconnect interface and prevent autoconnect
            disconnectInterface($arrConfig["connection.interface-name"]);
        }
        // POST SaveHotspotSettings
        if (isset($_POST['SaveHotspotSettings'])) {
            // Update hotspot connection settings and create alert message
            updateConnectionSettings($arrConfig, $status);
            // Start bridge
            if (isset($_POST['bridgedEnable'])) {
                startBridgedEnabled(
                    RASPI_NM_BRIDGE_MASTER_CONNECTION_NAME,
                    RASPI_NM_BRIDGE_MASTER_INTERFACE_NAME,
                    RASPI_NM_HOTSPOT_CONNECTION_NAME,
                    $_POST["wpa"],
                    $_POST["wpa_pairwise"],
                    $_POST["wpa_passphrase"],
                    $status,
                    $bridgedEnabled
                );
            }
            else {
                $result = stopBridgedEnabled(
                    RASPI_NM_BRIDGE_MASTER_CONNECTION_NAME,
                    RASPI_NM_BRIDGE_MASTER_INTERFACE_NAME,
                    RASPI_NM_HOTSPOT_CONNECTION_NAME,
                    $_POST["wpa"],
                    $_POST["wpa_pairwise"],
                    $_POST["wpa_passphrase"],
                    $status,
                    $bridgedEnabled
                );
            }
        }
        $_SESSION['status_messages'] = serialize($status);
        header("Location: ". $_SERVER['REQUEST_URI'], true, 303);
        exit;
    }

    // Check if connection is active
    $serviceStatus = checkConnectionIsActive(RASPI_NM_HOTSPOT_CONNECTION_NAME);

    echo renderTemplate(
        "hotspot_nm", compact(
            "status",
            "interfaces",
            "arrConfig",
            "arr80211Standard",
            "serviceStatus",
            "arrEncType",
            "arrSecurity",
            "bridgedEnabled",
            "hotspot_ifname"
        )
    );
}

/**
 * Reads the settings from the $_POST variable and updates them with nmcli call.
 * It also checks for open security and unsets unnecessary settings. Also sets
 * appropriate messages.
 * 
 * @param array &$settings array with the settings
 * @param StatusMessages $status StatusMessages object to add messages
 * @return array $settings the hotspot settings
 */
function updateConnectionSettings($settings, $status)
{
    $settings["connection.interface-name"] = $_POST["interface"];
    $settings["802-11-wireless.ssid"] = $_POST["ssid"];
    $settings["802-11-wireless.band"] = $_POST["hw_mode"];
    $settings["802-11-wireless.channel"] = $_POST["channel"];
    $settings["802-11-wireless.hidden"] = $_POST["hidden"];
    $settings["802-11-wireless-security.key-mgmt"] = $_POST["wpa"];
    $settings["802-11-wireless-security.pairwise"] = $_POST["wpa_pairwise"];
    $settings["802-11-wireless-security.psk"] = $_POST["wpa_passphrase"];

    // Check if Hotspot is open
    if (!strcmp($settings['802-11-wireless-security.key-mgmt'],
                "open")) {
        unset($settings["802-11-wireless-security.key-mgmt"]);
        unset($settings["802-11-wireless-security.pairwise"]);
        unset($settings["802-11-wireless-security.psk"]);
        setConnectionSecurityOpen(RASPI_NM_HOTSPOT_CONNECTION_NAME);
    }

    $result = setConnectionSettings(RASPI_NM_HOTSPOT_CONNECTION_NAME,
                                    $settings);

    // Add appropriate message alert
    if ($result) {
        $status->addMessage("Error saving settings: " . $result,
                            'danger');
    }
    else {
        $status->addMessage("Settings Saved", 'info');
    }

    return $settings;
}

/**
 * Checks if an ethernet connection is active and activates the master bridge if
 * it is. When there is no active ethernet connections it adds a status message
 * and returns.
 * @param string $master_con_name connection name of the master bridge
 * @param string $master_ifname interface name of master bridge
 * @param string $con_name connection name of the connection to become enslaved
 * @param string $key_mgmt
 * @param string $pairwise
 * @param string $psk the password of the connection to be enslaved if wifi connection with wifi-sec
 * @param StatusMessages $status StatusMessages object to save new status messages
 * @param int &$bridgedEnabled bridge state flag
 */
function startBridgedEnabled(
    $master_con_name,
    $master_ifname,
    $con_name,
    $key_mgmt,
    $pairwise,
    $psk,
    $status,
    &$bridgedEnabled
) {
    // If bridge already active do nothing
    if ($bridgedEnabled) {
        return;
    }

    $eth_con_name = getActiveConnection(RASPI_NM_ETHERNET_INTERFACE_NAME);

    if (!$eth_con_name) {
        $status->addMessage("No active ethernet connection, bridge activation failed", 'danger');
        return;
    }

    $result = startEthernetBridge(
        $master_con_name,
        $master_ifname,
        $eth_con_name,
        $con_name,
        $key_mgmt,
        $pairwise,
        $psk
    );

    if(strpos($result, "Error:")) {
        $status->addMessage("Bridge activation failed: $result", 'danger');
        $bridgedEnabled = 0;
    }
    else {
        $status->addMessage("Bridge activated", 'info');
        $bridgedEnabled = 1;
    }
}

/**
 * Checks if a bridge connection is active and deactivates the master bridge if
 * it is.
 * 
 * @param string $master_con_name connection name of the master bridge
 * @param string $master_ifname interface name of master bridge
 * @param string $con_name connection name of the connection to become wifi ap
 * @param string $key-mgmt
 * @param string $pairwise
 * @param string $psk the password of the connection wifi connection with wifi-sec
 * @param StatusMessages $status StatusMessages object to save new status messages
 * @param int &$bridgedEnabled bridge state flag
 */
function stopBridgedEnabled(
    $master_con_name,
    $master_ifname,
    $con_name,
    $key_mgmt,
    $pairwise,
    $psk,
    $status,
    &$bridgedEnabled
) {
    // If bridge not active do nothing
    if (!$bridgedEnabled) {
        return;
    }

    $eth_con_name = getActiveConnection(RASPI_NM_ETHERNET_INTERFACE_NAME);

    $result = stopEthernetBridge(
        $master_con_name,
        $master_ifname,
        $eth_con_name,
        $con_name,
        $key_mgmt,
        $pairwise,
        $psk
    );

    if(strpos($result, "Error:")) {
        $status->addMessage("Bridge deactivation failed: $result", 'danger');
        $bridgedEnabled = 1;
    }
    else {
        $status->addMessage("Bridge deactivated", 'info');
        $bridgedEnabled = 0;
    }
}

/**
 * Returns the last n lines of journalctl for Network Manager service in reverse
 * order (newest first) containing hotspots interface name. The number of lines
 * can be passed as an argument with default value 100. Number of lines should
 * be a natural number.
 * 
 * @param int $number_of_lines number of journal lines
 * @return array $journal the journalctl output
 */
function getNMJournal($number_of_lines = 300)
{
    $hotspot_ifname = getConnectionSettings(RASPI_NM_HOTSPOT_CONNECTION_NAME,
        ["connection.interface-name"])["connection.interface-name"];

    exec("journalctl -r -u NetworkManager.service -n $number_of_lines \
	    --since \"1 days ago\" 2>&1 |
        grep $hotspot_ifname", $journal);
    
    return $journal;
}
