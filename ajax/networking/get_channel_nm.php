<?php

require '../../includes/nm_functions.php';
require '../../includes/config.php';

$arrConfig = getConnectionSettings(RASPI_NM_HOTSPOT_CONNECTION_NAME);

$channel = intval($arrConfig['802-11-wireless.channel']);
echo json_encode($channel);