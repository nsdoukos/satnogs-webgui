<?php

require_once '../../includes/nm_functions.php';
session_start();

$json = json_decode(file_get_contents('php://input'), true);

$result = disconnectInterface($_SESSION['wifi_client_interface']);

if (strpos($result, "Error") !== false) {
    header("HTTP/1.0 400 Bad Request");
    echo $result;
}
