<?php
require_once '../../includes/functions.php';
require_once '../../includes/nm_functions.php';

session_start();

$clientInterface = $_SESSION['wifi_client_interface'];
$apInterface = $_SESSION['ap_interface'];
$nearbyNetworks = getAvailableWifiNetworks($clientInterface);
$knownNetworks = getKnownWifiNetworks();
$connectedSSID = getConnectedSSID($clientInterface);

// Combine known and nearby networks
$common_keys = array_keys(
  array_intersect_key($knownNetworks, $nearbyNetworks)
  );

foreach ($common_keys as $key) {
  foreach ($nearbyNetworks[$key] as $key2 => $elem) {
    $knownNetworks[$key][$key2] = $elem;
  }
  foreach ($knownNetworks[$key] as $key2 => $elem) {
    $nearbyNetworks[$key][$key2] = $elem;
  }
}
// Add index field to network arrays
$index = 0;
foreach ($knownNetworks as &$network) {
  $network["index"] = $index++;
}
unset($network);

$index = 0;
foreach ($nearbyNetworks as &$network) {
  $network["index"] = $index++;
}
unset($network);

$connectedNetwork = $nearbyNetworks[$connectedSSID];

?>

<?php if (!empty($connectedNetwork)): $network = $connectedNetwork; ?>
  <div class="row" id="connected_stations_nm">
    <div class="col">
      <h4 class="h-underlined my-3"><?php echo _("Connected") ?></h4>
      <div class="card-grid">
        <?php echo renderTemplate("wifi_stations/network_nm",
                                  compact ("network"))?>
      </div>
    </div>
  </div>
<?php endif ?>
<?php if (!empty($nearbyNetworks)): ?>
  <div class="row" id="nearby_stations_nm">
    <div class="col">
      <h4 class="h-underlined my-3"><?php echo _("Nearby") ?></h4>
      <div class="card-grid">
        <?php foreach ($nearbyNetworks as $network): ?>
          <?php echo renderTemplate("wifi_stations/network_nm",
                                     compact ("network"))?>
        <?php endforeach?>
      </div>
    </div>
  </div>
<?php endif ?>
<?php if (!empty($knownNetworks)): ?>
  <div class="row" id="known_stations_nm">
    <div class="col">
      <h4 class="h-underlined my-3"><?php echo _("Known") ?></h4>
      <div class="card-grid">
        <?php foreach ($knownNetworks as $network): ?>
           <?php echo renderTemplate("wifi_stations/network_nm",
                                      compact ("network"))?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
<?php endif ?>
