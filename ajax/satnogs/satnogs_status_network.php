<?php
require_once "../../config/config.php";
session_start();

$opts = array('https' => array (
                'method' => 'HEAD'));

$header_check = get_headers($_SESSION['satnogs_network_url'],
                            false, 
                            stream_context_create($opts));

echo $header_check[0] ? $header_check[0] : "Not found";
